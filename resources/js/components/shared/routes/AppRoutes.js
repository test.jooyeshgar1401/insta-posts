import React from "react";
import { BrowserRouter as Router, Routes , Route } from "react-router-dom";
import Dashboard from "../../Dashboard/Dashboard";
import PostConrainer from "../../Posts/PostConrainer";

const AppRoutes = () => (
    <Router>
        <Routes  >
            <Route exact path="/" element={<Dashboard />}  ></Route>
            <Route exact path="/posts" element={<PostConrainer />}  ></Route>
        </Routes>
    </Router>
);

export default AppRoutes;