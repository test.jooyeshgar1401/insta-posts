import axios from "axios";
const basedURL = '/api/';

const handleSuccess = ({ response , type , next, reduxData }) => {
    next({
        type : type,
        data : response.data.data,
        ...response,
        ...reduxData
    })

    return new Promise( (resolve , reject) => {
        resolve(response);
    });
};

const handleError = ({ error , type , next }) => {
    next({
        type,
        error
    })
    return new Promise( (resolve , reject) => {
        reject(error);
    });
};

const apiMiddleware = store => next =>action => {

    const {isEndPointCall , type , successType ,errorType , reduxData={} } = action;
    console.log('apiMiddleware')

    if(isEndPointCall){

        next({type});
        const {method} = action;

        return axios(`${basedURL}${action.endpoint}`,{
            method,
        }).then(response => handleSuccess({response , type : successType , next, reduxData}) )
        .catch(error => handleError({error , type : errorType , next}) )
        ; 
    }
    else{
        next(action);
    }
};

export default apiMiddleware;