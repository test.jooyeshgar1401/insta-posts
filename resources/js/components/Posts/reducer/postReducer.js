import { Types } from "../actions/actionTypes";

const initState = {
    error : null,
    posts : [],
    total : 0,
    fetching : false,
    deleteing : false,
    deleteError:null
}

export default function postReducer ( state = initState , action ){
    
    switch(action.type){

        case Types.FETCH_POSTS : 
            console.log('FETCH_POSTS')
            return {
                ...state , 
                fetching : true
            }
        break;

        case Types.FETCH_POSTS_SUCCESS : 
        console.log('FETCH_POSTS_SUCCESS')
            return {
                ...state , 
                total : action.data.total,
                fetching : false,
                posts : action.data.data
            }
        break;

        case Types.FETCH_POSTS_FAILED : 

            return {
                ...state , 
                fetching : false,
                error : action.error
            }
        break;

        case Types.DELETE_POST: 
        console.log('DELETE_POST')
            
            return {
                ...state , 
                deleteing : true
            }
        break;

        case Types.DELETE_POST_SUCCESS :         
        console.log('DELETE_POST_SUCCESS')

            return {
                ...state , 
                deleteing : false,
                posts : state.posts.filter(post => post.id!=action.id)
            }
        break;

        case Types.DELETE_POST_FAILED : 

            return {
                ...state , 
                deleteing : false,
                deleteError : action.error
            }
        break;
        default:
            return state;
    }
}