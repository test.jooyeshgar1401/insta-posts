import React , {Component} from "react";
import axios from "axios";
import { connect } from "react-redux";
import { fetchPosts , deletePostAndFetch } from "./actions/postsActions";
import PostList from "./PostList";
import Pagination from "./Pagination";

class PostConrainer extends Component {

    state = {
        _limit : 9,
        _page : 1
    };

    get filters (){
        const {_limit , _page} = this.state;
        return {_limit , _page};
    }

    fetchPosts = () => this.props.fetchPosts(this.filters);

    onPageChange = newPage => this.setState({_page : newPage} , this.fetchPosts );

    deletePost = (id) => {
console.log('PostConrainer:deletePost')

        const { filters } = this;
        const { deletePostAndFetch } = this.props;

        deletePostAndFetch(id, filters);
    }
    componentDidMount(){

        /*axios('/api/posts').then(response => {
            this.setState({posts : [...response.data]})
        })
        .catch(error => {
            console.log(error)
        });*/
console.log('PostConrainer:fetchPosts')

        this.fetchPosts();

    }
    render(){
        
        const {posts , total } = this.props;
        const {_limit , _page} = this.state;
        const {onPageChange , deletePost} = this;
        const PaginationComponent = <Pagination onPageChange={onPageChange} total={total} limit={_limit} page={_page} />
        /*return <div>
            <h1>Posts</h1>
            {this.state.posts.map(post => (
                <div key={post.id} >{post.body}</div>
            ))}
        </div>*/

        return <div>
            <h1>Posts</h1>
            {PaginationComponent}
            <PostList posts={posts} deletePost={deletePost} />            
            {PaginationComponent}
        </div>
    }
}

const mapStateToProps = ({ posts, fetching, error , total }) => ({
    posts,
    fetching,
    error,
    total
  });

const mapDispathToProps = {
    fetchPosts,
    deletePostAndFetch
}

export default connect(mapStateToProps , mapDispathToProps)(PostConrainer);