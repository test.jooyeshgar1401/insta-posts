import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

const PostItem = ({ post , deletePost }) => {

    const onDeletePost = () => {

        if(window.confirm('Are you sure')){
            deletePost(post.id);
        }
    };
  return (
    <div className="col-md-6 col-xl-4 mb-5">
        <a className="card post-preview lift h-100" href="#!">
            <img className="card-img-top" src={"img/blog/" + post.image} alt={post.title} />
            <div className="card-body">
                <h5 className="card-title">{post.title}</h5>
                <p className="card-text">{post.body}</p>
            </div>
            <div className="card-footer">
                <div className="post-preview-meta">
                    <img className="post-preview-meta-img" src={"img/" + post.user_image} />
                    <div className="post-preview-meta-details">                    
                        <div className="post-preview-meta-details-name">Aariz Fischer</div>
                        <div className="post-preview-meta-details-date">Feb 4 · 5 min read</div>
                    </div>
                    <div className="post-preview-meta-details ml-auto"  >
                        <button className="btn" onClick={onDeletePost} ><FontAwesomeIcon icon={faTrash} /></button>
                    </div>
                </div>
            </div>
        </a>
    </div>
  );
};

export default PostItem;
